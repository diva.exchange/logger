# Modern, fast and lightweight Logger

Modern, fast, lightweight and generic logger. It's based on pino (https://www.npmjs.com/package/pino).

## How to Install
    npm install @diva.exchange/diva-logger

## Getting Started - Usage
Use it either as

    import { Logger } from '@diva.exchange/diva-logger'

    Logger.trace('hello world')
    
or

    const Logger = require('@diva.exchange/diva-logger')

    Logger.trace('hello world')


## API

diva-logger is static. It can be used without instantiation, like
 
    import { Logger } from '@diva.exchange/diva-logger' 
    
    try  {
        throw new Error('A recoverable error')
    } catch (error) {
        Logger.warn(error)
        // go on
    }   

diva-logger also attaches itself gently to the two global events `uncaughtException` and `unhandledRejection`. Hence an unhandled exception or an unhandled rejected promise will be properly logged (fatal):

    import { Logger } from '@diva.exchange/diva-logger' 
    
    throw new Error('This is fatal')

After an unhandled exception or unhandled rejected promise, the process will be terminated with the exit code 1 (after the remaining event stack has been processed).

### Chaining
diva-logger supports chaining, like 
   
    Logger.trace('trace message').debug('debug message')
   
### Logging Examples, Usage of API

#### trace(any[, options])

    Logger.trace('some trace message')
    Logger.trace(process.env)

#### debug(any[, options])

    Logger.debug(12345)

#### info(any[, options])

    Logger.info('info goes here')

#### warn(any[, options])

    Logger.warn('warning - recoverable error')

#### error(any[, options])

    Logger.error('some serious error')
    Logger.error(SomeObject)

#### fatal(any[, options])

    Logger.fatal('One of the last messages of the process...')

## How to Run Unit Tests
    npm run test

Coverage reports are stored within `./coverage`. Use any browser and open `./coverage/index.html`.

## How to Lint (eslint)
    npm run lint

## Contact the Developers

On [DIVA.EXCHANGE](https://www.diva.exchange) you'll find various options to get in touch with the team. 

Talk to us via Telegram [https://t.me/diva_exchange_chat_de]() (English or German).

## Donations

Your donation goes entirely to the project. Your donation makes the development of DIVA.EXCHANGE faster.

XMR: 42QLvHvkc9bahHadQfEzuJJx4ZHnGhQzBXa8C9H3c472diEvVRzevwpN7VAUpCPePCiDhehH4BAWh8kYicoSxpusMmhfwgx

BTC: 3Ebuzhsbs6DrUQuwvMu722LhD8cNfhG1gs

Awesome, thank you!
