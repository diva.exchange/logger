/**
 * Copyright (C) 2021 diva.exchange
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Author/Maintainer: Konrad Bächler <konrad@diva.exchange>
 */

'use strict'

import pino from 'pino'
import util from 'util'

const LOGGER_LEVELS = {
  trace: 10,
  debug: 20,
  info: 30,
  warn: 40,
  error: 50,
  fatal: 60
}

export class Logger {
  /**
   * Log a fatal error
   *
   * @param message
   * @param options {Object} See https://github.com/pinojs/pino/blob/HEAD/docs/api.md
   * @return {Logger}
   * @public
   */
  static fatal (message, options) {
    return Logger._log(LOGGER_LEVELS.fatal, message, options)
  }

  /**
   * Log an error
   *
   * @param message
   * @param options {Object} See https://github.com/pinojs/pino/blob/HEAD/docs/api.md
   * @return {Logger}
   * @public
   */
  static error (message, options) {
    return Logger._log(LOGGER_LEVELS.error, message, options)
  }

  /**
   * Log a warning
   *
   * @param message
   * @param options {Object} See https://github.com/pinojs/pino/blob/HEAD/docs/api.md
   * @return {Logger}
   * @public
   */
  static warn (message, options) {
    return Logger._log(LOGGER_LEVELS.warn, message, options)
  }

  /**
   * Log an information
   *
   * @param message
   * @param options {Object} See https://github.com/pinojs/pino/blob/HEAD/docs/api.md
   * @return {Logger}
   * @public
   */
  static info (message, options) {
    return Logger._log(LOGGER_LEVELS.info, message, options)
  }

  /**
   * Log a debug message
   *
   * @param message
   * @param options {Object} See https://github.com/pinojs/pino/blob/HEAD/docs/api.md
   * @return {Logger}
   * @public
   */
  static debug (message, options) {
    return Logger._log(LOGGER_LEVELS.debug, message, options)
  }

  /**
   * Log a trace message
   *
   * @param message
   * @param options {Object} See https://github.com/pinojs/pino/blob/HEAD/docs/api.md
   * @return {Logger}
   * @public
   */
  static trace (message, options) {
    return Logger._log(LOGGER_LEVELS.trace, message, options)
  }

  /**
   * @param options {Object} See https://github.com/pinojs/pino/blob/HEAD/docs/api.md
   * @return {Logger}
   * @public
   */
  static setOptions (options) {
    Logger._make(options)
    return Logger
  }

  /**
   * Log a message
   *
   * @param level {number}
   * @param message
   * @param options {Object}
   * @return {Logger}
   * @private
   */
  static _log (level, message, options) {
    if (!Logger._pino || (typeof options !== 'undefined' && options !== Logger._options)) {
      Logger._make(options)
    }

    switch (typeof message) {
      case 'string':
        break
      case 'undefined':
        message = '[undefined]'
        break
      case 'boolean':
        message = message ? '(boolean)TRUE' : '(boolean)FALSE'
        break
      default:
        message = util.format('%o', message)
    }

    switch (level) {
      case LOGGER_LEVELS.trace:
        Logger._pino.trace(message)
        break
      case LOGGER_LEVELS.debug:
        Logger._pino.debug(message)
        break
      case LOGGER_LEVELS.info:
        Logger._pino.info(message)
        break
      case LOGGER_LEVELS.error:
        Logger._pino.error(message)
        break
      case LOGGER_LEVELS.fatal:
        Logger._pino.fatal(message)
        break
      // default warning
      default:
        Logger._pino.warn(message)
    }

    return Logger
  }

  /**
   * @param options
   * @returns {pino}
   * @private
   */
  static _make (options) {
    if (typeof Logger._hasPrettyPrint === 'undefined') {
      Logger._hasPrettyPrint = false
      try {
        require.resolve('pino-pretty')
        Logger._hasPrettyPrint = true
      } catch (e) {
      }
    }

    Logger._defaultOptions(options)

    Logger._pino = pino(Logger._options)

    if (!process.listeners('uncaughtException').includes(Logger._genericExceptionHandler)) {
      process.prependOnceListener('uncaughtException', Logger._genericExceptionHandler)
    }
    if (!process.listeners('unhandledRejection').includes(Logger._genericExceptionHandler)) {
      process.prependOnceListener('unhandledRejection', Logger._genericExceptionHandler)
    }

    return Logger._pino
  }

  /**
   * @param options
   * @private
   */
  static _defaultOptions (options) {
    if (!Logger._optionsDefault) {
      const base = {}
      const nameDefault = process.mainModule.filename
      const levelDefault = process.env.LOG_LEVEL ||
        (process.env.NODE_ENV === 'production' ? /* istanbul ignore next */ 'info' : 'trace')
      const prettyPrintDefault = Logger._hasPrettyPrint
        ? { translateTime: true }
        : /* istanbul ignore next */ false

      Logger._optionsDefault = {
        base: base,
        name: nameDefault,
        level: levelDefault,
        prettyPrint: prettyPrintDefault
      }
    }

    if (typeof options === 'undefined') {
      options = Logger._optionsDefault
    } else {
      options.base = options.base || Logger._optionsDefault.base
      options.name = options.name || Logger._optionsDefault.name
      options.level = options.level || Logger._optionsDefault.level
      options.prettyPrint = Logger._hasPrettyPrint
        ? options.prettyPrint || Logger._optionsDefault.prettyPrint
        : /* istanbul ignore next */ false
    }

    Logger._options = options
  }

  /**
   * Generic global exception handler
   *
   * @param error
   * @param origin
   * @public
   */
  /* istanbul ignore next */
  static _genericExceptionHandler (error, origin) {
    if (typeof origin !== 'undefined') {
      Logger.fatal(origin)
    }
    Logger.fatal(error)

    // async exit, after the remaining event stack has been processed
    setImmediate(() => { process.exit(1) })
  }
}

module.exports = { Logger }
