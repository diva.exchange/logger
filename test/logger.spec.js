/**
 * Copyright (C) 2021 diva.exchange
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Author/Maintainer: Konrad Bächler <konrad@diva.exchange>
 */

'use strict'

import { Logger } from '../src/logger.js'

import { assert } from 'chai'
import { describe, it, beforeEach } from 'mocha'

/**
 * Project: logger
 * Context: logger
 */
describe('//logger// /logger', function () {
  describe('Logging functions', function () {
    it('Trace', function () {
      assert.deepEqual(Logger.trace('trace'), Logger)
    })
    it('Debug', function () {
      assert.deepEqual(Logger.debug('debug'), Logger)
    })
    it('Info', function () {
      assert.deepEqual(Logger.info('info'), Logger)
    })
    it('Warn', function () {
      assert.deepEqual(Logger.warn('warn'), Logger)
    })
    it('Error', function () {
      assert.deepEqual(Logger.error('error'), Logger)
    })
    it('Fatal', function () {
      assert.deepEqual(Logger.fatal('fatal'), Logger)
    })
    it('Trace undefined', function () {
      assert.deepEqual(Logger.trace(undefined), Logger)
    })
    it('Trace boolean', function () {
      assert.deepEqual(Logger.trace(true).trace(false), Logger)
    })
    it('Trace object, pretty print', function () {
      process.env.NODE_ENV = 'development'
      const a = { a: 'b' }
      const b = { a: a, arr: [1, { x: a }] }
      b.b = b
      assert.deepEqual(Logger.trace(b), Logger)
    })
  })
  describe('Chaining', function () {
    it('trace.fatal chain', function () {
      assert.deepEqual(Logger.trace('trace').fatal('fatal'), Logger)
    })
  })
  describe('Options', function () {
    beforeEach('clean up Logger', function () {
      Logger._options = Logger._optionsDefault = Logger._pino = undefined
    })

    it('setOptions', function () {
      assert.deepEqual(Logger.setOptions(), Logger)
      assert.deepEqual(Logger.setOptions({ timestamp: false }), Logger)
    })
    it('log with options', function () {
      assert.deepEqual(Logger.trace('log as test', { name: 'test' }), Logger)
      assert.deepEqual(Logger.trace('log as other-test', { name: 'other-test' }), Logger)
    })
  })
})
